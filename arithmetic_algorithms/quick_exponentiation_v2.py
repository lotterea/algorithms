def _pow(x, n):
    if n == 0:
        return 1
    res = 1
    while n > 0:
        if n % 2 == 1:
            res = res * x
            n = n - 1
        x = x * x
        n = n // 2
    return res


if __name__ == "__main__":
    print(_pow(int(input("x: ")), int(input("pow: "))))
