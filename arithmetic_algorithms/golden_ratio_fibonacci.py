import math


def golden_ratio_fibonacci(n):
    return int((((1 + math.sqrt(5)) ** n) - ((1 - math.sqrt(5))) ** n) / (2 ** n * math.sqrt(5)))


if __name__ == "__main__":
    print(golden_ratio_fibonacci(int(input("N: "))))
