def _pow(x, n):
    if n == 0:
        return 1
    res = x
    p = 1
    while p * 2 < n:
        p = p * 2
        res = res * res
    for _ in range(n - p):
        res = res * x
    return res


if __name__ == "__main__":
    print(_pow(int(input("x: ")), int(input("pow: "))))
