def count_primes(n):
    q = 2
    for i in range(4, n):
        if is_prime(i):
            q += 1
    return q


def is_prime(n):
    if n > 1:
        for i in range(2, n):
            if n % i == 0:
                return False
        return True


if __name__ == "__main__":
    print(count_primes(int(input("N: "))))
