import math


def sieve_of_eratosthenes(n):
    a = [i for i in range(n + 1)]
    a[1] = 0
    i = 2
    s = math.sqrt(n)
    while i <= s:
        if a[i] != 0:
            j = i + i
            while j <= n:
                a[j] = 0
                j = j + i
        i = i + 1
    a = [i for i in a if a[i] != 0]
    return len(a)


if __name__ == "__main__":
    print(sieve_of_eratosthenes(int(input("n: "))))
